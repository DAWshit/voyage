window.onscroll = function() {scrollBar()};

function scrollBar() {
	const bar = document.querySelector(".headnav");
	if ($(window).scrollTop() > $(window).height()) {
		bar.style.cssText += "visibility: hidden;";
	} else {
		bar.style.removeProperty("visibility")
	}
}

function scrollSide() {
	const side = document.querySelector(".sidebar");
	if (side.style.left != "0px") {
		side.style.left = "0px";
	} else {
		side.style.left = "-322px";
	}
}

function rndDest() {
	let rndm = Math.floor(Math.random() * 4);
	let link = "";
	switch(rndm){
		case 0:
			link = "bds.html";
			break;
		case 1:
			link = "fji.html";
			break;
		case 2:
			link = "jpn.html";
			break;
		case 3:
			link = "mlt.html";
			break;
	}
	location.href=link;
}
